package ru.smlab.school.day20.pens;

import java.util.Comparator;

public class ReversedAlphabetComparator implements Comparator<Pen> {

    @Override
    public int compare(Pen o1, Pen o2) {
        return Comparator.<String>reverseOrder().compare(o1.brand, o2.brand);
    }
}
