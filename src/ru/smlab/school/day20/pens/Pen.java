package ru.smlab.school.day20.pens;

public class Pen implements Comparable<Pen>{
    String brand;
    int color;

    public Pen(String brand, int color) {
        this.brand = brand;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Pen{" +
                "brand='" + brand + '\'' +
//                ", color=" + Integer.toHexString(color) +
                ", color=" + String.format("0x%06X",color) +
                '}';
    }

    @Override
    public int compareTo(Pen p) {
        return this.brand.compareTo(p.brand);
    }

}
