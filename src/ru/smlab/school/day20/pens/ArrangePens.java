package ru.smlab.school.day20.pens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Задание 20.0.1
 * Заведующий магазина канцелярских товаров - перфекционист. По четным дням недели он выкладывает ручки в алфавитном порядке брендов, по нечетным дням недели он выкладывает их в обратном порядке, а на выходных - в цветовом порядке - от самых светлых оттенков к самым темным.
 * Реализуйте классы и логику для каждой из сортировок с применением интерфейсов Comparator - Comparable.
 */
public class ArrangePens {
    public static void main(String[] args) {
        List<Pen> pens = new ArrayList<>();
        pens.add(new Pen("bic", 0x000000));
        pens.add(new Pen("pentel", 0x0000ff));
        pens.add(new Pen("lamy", 0x00ff00));
        pens.add(new Pen("stabilo", 0xff0000));

        System.out.println(pens);
        Collections.sort(pens);
        System.out.println(pens);

        ReversedAlphabetComparator comp1 = new ReversedAlphabetComparator();
        Collections.sort(pens, comp1);
        System.out.println(pens);

        ColorComparator comp2 = new ColorComparator();
        Collections.sort(pens, comp2);
        System.out.println(pens);

    }
}
