package ru.smlab.school.day20.pens;

import java.util.Comparator;

public class ColorComparator implements Comparator<Pen> {
    @Override
    public int compare(Pen o1, Pen o2) {
        return Integer.compare(o1.color, o2.color);
//        return new Integer(o1.color).compareTo(o2.color);
    }
}
