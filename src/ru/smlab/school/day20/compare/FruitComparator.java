package ru.smlab.school.day20.compare;

import java.util.Comparator;
import java.util.Objects;

public class FruitComparator implements Comparator<Fruit> {
    @Override
    public int compare(Fruit o1, Fruit o2) {
        return o1.name.compareTo(o2.name);
    }
}
