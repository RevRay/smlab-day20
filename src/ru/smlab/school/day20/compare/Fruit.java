package ru.smlab.school.day20.compare;

public class Fruit implements Comparable<Fruit>{
    String name;
    int weight;

    public Fruit(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Fruit{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Fruit f) {
        return Integer.compare(this.weight, f.weight);
//        return this.weight > f.weight ? 1 : this.weight < f.weight ? -1 : 0;
    }
}
