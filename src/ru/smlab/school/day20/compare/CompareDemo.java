package ru.smlab.school.day20.compare;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CompareDemo {
    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(2);
        ints.add(20);
        ints.add(5);
        ints.add(4);
        ints.add(-90);
        ints.add(28);

        System.out.println(ints);
        Collections.sort(ints);
        System.out.println(ints);

        List<String> strs = Arrays.asList("fsdg", "aaa", "xxds");
        Collections.sort(strs);

        Integer i1 = 9099;
        Integer i2 = 109;
        System.out.println("comparint ints:" + i1.compareTo(i2));

        List<Fruit> fruits = Arrays.asList(
                new Fruit("banana", 100),
                new Fruit("apple", 90),
                new Fruit("watermelon", 10),
                new Fruit("mango", 120)
        );
        System.out.println(fruits);
        Collections.sort(fruits);
        System.out.println(fruits);

        FruitComparator comp = new FruitComparator();
        fruits.sort(comp);
//        Collections.sort(fruits, comp);
        System.out.println(fruits);


    }
}
