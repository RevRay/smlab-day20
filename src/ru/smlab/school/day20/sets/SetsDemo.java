package ru.smlab.school.day20.sets;

import java.util.HashSet;
import java.util.Set;

public class SetsDemo {
    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>();
        set1.add("Малинова Наталья");
        set1.add("Ежевичная Светлана");
        set1.add("Ягодкина Мария");
        set1.add("Малинова Наталья");

        System.out.println("set1: " + set1);

        Set<String> set2 = new HashSet<>();
        set2.add("Ягодкина Мария");
        set2.add("Малинова Наталья");
        set2.add("Черешнева Ольга");
        set2.add("Черешнева Ольга");
        set2.add("Вишневская Дарья");

        System.out.println("set2: " + set2);

        //TODO set operations

    }
}
