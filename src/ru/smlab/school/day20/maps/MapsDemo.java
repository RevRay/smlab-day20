package ru.smlab.school.day20.maps;

import java.util.HashMap;
import java.util.Map;

public class MapsDemo {
    //List O(n) Set O(log n)
    public static void main(String[] args) {
        Map<String, Car> maps = new HashMap<>();

        maps.put("А456ВТ770", new Car("TOyota corolla", "red"));
        maps.put("А456ВТ779", new Car("TOyota corolla", "blue"));
        maps.put("А456ВТ772", new Car("TOyota corolla", "blue"));
        maps.put("А456ВТ770", new Car("TOyota corolla", "green"));

        System.out.println(maps);

//        for (Car c : maps) {
//
//        }
        for (Map.Entry<String, Car> c : maps.entrySet()) {
            System.out.println("###" + c);
        }
    }
}
