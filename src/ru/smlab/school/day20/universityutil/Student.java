package ru.smlab.school.day20.universityutil;

import java.util.List;
import java.util.Objects;

public class Student {
    String fio;
    List<Integer> scores; //4,5,5
    int absenceCount;
    double avgScore;
    boolean graduated;

    public double recalculateAvgScore(){
        double scoreSum = 0.0;
        for (Integer score : scores){
            scoreSum += score;
        }
        double avgScore = scoreSum / scores.size();
        this.avgScore = avgScore;
        return avgScore;
    }

    public Student(String fio, List<Integer> scores, int absenceCount) {
        this.fio = fio;
        this.scores = scores;
        this.absenceCount = absenceCount;
        this.avgScore = recalculateAvgScore();
        this.graduated = false;
    }

    public Student(String fio, List<Integer> scores, int absenceCount, boolean graduated) {
        this.fio = fio;
        this.scores = scores;
        this.absenceCount = absenceCount;
        this.graduated = graduated;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fio='" + fio + '\'' +
                ", scores=" + scores +
                ", absenceCount=" + absenceCount +
                ", avgScore=" + avgScore +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(fio, student.fio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fio);
    }
}
