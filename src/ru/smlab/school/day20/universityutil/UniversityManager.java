package ru.smlab.school.day20.universityutil;

import java.util.*;

/**
 * университету необходима программа, оценивающая успеваемость студентов. Сейчас же есть только база, хранящая абсолютно всех когда-либо учившихся учеников.
 * Программа должна уметь формировать данные (ФИО) о:
 * 1 - всех выпускниках
 * 2 всех отличниках (средний балл > 4.5) ещепродолжают обучение
 * 3 - всех дисциплинированных студентах (м1аксимум 1 пропусков занятий)
 * всех хорошистах
 * всех студентах, получающих стипендию (хорошист без пропусков занятий или отличник с пропусками) //addAll()
 * всех студентах, получающих повышенную стипендию (отличник без пропусков)
 */
public class UniversityManager {

    static Random rnd = new Random();

    public static void main(String[] args) {
        Set<Student> allStudents = new HashSet<>();

        allStudents.add(new Student("Аристарх1", List.of(5,5,4), 1));
//        allStudents.add(new Student("Аристарх1", List.of(5,4,4), 1));
        allStudents.add(new Student("Аристарх", generateMarks(3, 5), 1));
        allStudents.add(new Student("Кондратий", generateMarks(3, 5), 0));
        allStudents.add(new Student("Вильгельм", generateMarks(3, 5), 1, true));
        allStudents.add(new Student("Маврикий", generateMarks(4, 5), 0));
        allStudents.add(new Student("Бронислава", generateMarks(4, 5), 1));
        allStudents.add(new Student("Пафнутий", generateMarks(4, 5), 0, true));
        allStudents.add(new Student("Людмила", generateMarks(4, 5), 1));
        allStudents.add(new Student("Людмила", List.of(5, 4, 3), 1));

        System.out.println(allStudents);

        Set<Student> graduatedStudents = new HashSet<>();
        for (Student s : allStudents) {
            if (s.graduated) {
                graduatedStudents.add(s);
            }
        }
        System.out.println("Список студентов окончивших ВУЗ");
        System.out.println(graduatedStudents);


        System.out.println("До " + allStudents);
        Set<Student> presentStudents = new HashSet<>(allStudents);
        presentStudents.removeAll(graduatedStudents);
        //        for (Student s : allStudents) {
//            if (!s.graduated) {
//                presentStudents.add(s);
//            }
//        }
        System.out.println("После: "+presentStudents);


        Set<Student> bigScoreStudents = new HashSet<>();
        for (Student s : presentStudents) {
            if (s.avgScore >= 4.5) {
                bigScoreStudents.add(s);
            }
        }
        System.out.println("Отличники: " + bigScoreStudents);

        System.out.println(presentStudents.removeAll(bigScoreStudents));
    }

    public static List<Integer> generateMarks(int lowestMark, int amount) {
        List<Integer> generatedList = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            generatedList.add(rnd.nextInt(6 - lowestMark) + lowestMark);
        }
        return generatedList;
    }
}
